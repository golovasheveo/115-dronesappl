import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class DronesAppl {
    private static final int N_DRONES = 20;
    private static final int N_HEIGHTS = 15;
    private static final int MIN_PASSENGER_TIME = 1;
    private static final int MAX_PASSENGER_TIME = 15;
    private static final int MODEL_TIME = 240;
    static Drone[] drones;
    static List<Drone> dronesInAir;
    static List<Drone> dronesInQueue;
    static final String[] heights =
            {"I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX",
                    "X", "XI", "XII", "XIII", "XIV", "XV"};
    static final HashMap<String, Integer> heightsCounts = new HashMap<>();

    public static void main(String[] args) {
        preProcessing();
        play();
        postProcessing();
        testApplication();
        displayResults();
    }

    private static void testApplication() {
        int totalPassengers = 0;
        for (Drone drone : drones) {
            totalPassengers += drone.getnPassengers();
            if (drone.getTotalAirIterations() + drone.getTotalQueueIterations() != MODEL_TIME) {
                throw new RuntimeException("Error 1");
            }
        }
        if (totalPassengers != GetTotalFlights()) {
            throw new RuntimeException("Error 2");
        }
    }

    private static void displayResults() {
        displayDrones();
        displayHeights();
        displayHeightsMaxFlights();
    }

    private static void displayHeightsMaxFlights() {
        int max;
        max = heightsCounts.values()
                .stream()
                .max(Integer::compareTo)
                .get();

        List<String> heights = heightsCounts.entrySet()
                .stream()
                .filter(e -> (e.getValue() == max))
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());

        System.out.println("The most used heights: " + heights);
    }

    private static void displayHeights() {
        heightsCounts.keySet()
                .forEach(height -> System.out.printf("Height %s: %d flights\n",
                        height, heightsCounts.get(height)));
    }

    private static void displayDrones() {
        Arrays.stream(drones).forEach(drone -> System.out.printf("Drone #%d was in air %d minutes. " +
                        "It transferred %d passengers. It was in waiting queue %d minutes\n",
                drone.getSeqNumber(),
                drone.getTotalAirIterations(),
                drone.getnPassengers(),
                drone.getTotalQueueIterations()
        ));
    }

    private static int GetTotalFlights() {
        return heightsCounts.values().stream().reduce(0, Integer::sum);
    }

    private static void postProcessing() {
        dronesInAir.forEach(drone ->
                drone.setTotalAirIterations(drone.getTotalAirIterations() + MODEL_TIME - drone.getStartIteration()));
        dronesInQueue.forEach(drone ->
                drone.setTotalQueueIterations(drone.getTotalQueueIterations() + MODEL_TIME - drone.getStartQueueIteration()));
    }

    private static void play() {
        for (int i = 1; i <= MODEL_TIME; i++) {
            List<String> freedHeights = landingOnIteration(i);
            takingOffOnIteration(i, freedHeights);
        }
    }

    private static void takingOffOnIteration(int nIteration, List<String> freedHeights) {
        int count = freedHeights.size();
        if (count > 0) {
            for (int i = 0; i < count; i++) {
                Drone drone = dronesInQueue.remove(0);
                takeOff(drone, nIteration, freedHeights.remove(0));
                drone.setTotalQueueIterations((nIteration - drone.getStartQueueIteration())
                        + drone.getTotalQueueIterations());
            }
        }
    }

    private static List<String> landingOnIteration(int nIteration) {
        List<String> res = new ArrayList<>();
        Iterator<Drone> it = dronesInAir.iterator();
        while (it.hasNext()) {
            Drone drone = it.next();

            if (drone.getFinishIteration() == nIteration) {
                it.remove();
                int timeInAir =
                        drone.getFinishIteration() - drone.getStartIteration();
                drone.setTotalAirIterations
                        (drone.getTotalAirIterations() + timeInAir);
                putInQueue(drone, nIteration);
                res.add(drone.getHeight());
            }
        }
        return res;
    }

    private static void preProcessing() {
        createDrones();
        initializeInAirInQueue();
        startDronesInAir();
        startDronesInQueue();
    }

    private static void initializeInAirInQueue() {
        dronesInAir = new LinkedList<>();
        dronesInQueue = new LinkedList<>();
    }

    private static void startDronesInQueue() {
        for (int i = N_HEIGHTS; i < N_DRONES; i++) {
            putInQueue(drones[i], 0);
        }
    }

    private static void putInQueue(Drone drone, int nIteration) {
        dronesInQueue.add(drone);
        drone.setStartQueueIteration(nIteration);
    }

    private static void startDronesInAir() {
        for (int i = 0; i < N_HEIGHTS; i++) {
            takeOff(drones[i], 0, heights[i]);
        }
    }

    private static void takeOff(Drone drone, int nIteration, String height) {
        dronesInAir.add(drone);
        drone.setStartIteration(nIteration);
        int iterationsInAir = getIterationsInAir();
        drone.setFinishIteration(nIteration + iterationsInAir);
        drone.setHeight(height);
        drone.setnPassengers(drone.getnPassengers() + 1);
        heightsCounts.merge(height, 1, Integer::sum);
    }

    private static int getIterationsInAir() {
        return (int) (MIN_PASSENGER_TIME +
                Math.random() * (MAX_PASSENGER_TIME -
                        MIN_PASSENGER_TIME + 1));
    }

    private static void createDrones() {
        drones = IntStream.range(0, N_DRONES)
                .mapToObj(i -> new Drone(i + 1))
                .toArray(Drone[]::new);
    }
}